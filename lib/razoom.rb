require 'active_resource'
require 'oauth2'
require 'razoom/api_collection'
require 'razoom/paginated_collection'
require 'razoom/product_collection'
require 'razoom/client'
require 'razoom/base'
require 'razoom/search'
require 'razoom/category'
require 'razoom/destination'
require 'razoom/country'
require 'razoom/location'
require 'razoom/availability'
require 'razoom/price'
require 'razoom/product'
require 'razoom/customer'
require 'razoom/cart'
require 'razoom/checkout'

module Razoom
  API_VERSION = 'v1'

  def self.initialize(config={})
    @api_endpoint ||= config[:endpoint]
    @oauth ||= OAuth2::Client.new(config[:app_id], config[:secret], site: base_uri)
    Razoom::Base.set_site(base_uri)
  end

  def self.generate_token(name, password)
    @oauth.password.get_token(name, password)
  end

  def self.base_uri
    "#{@api_endpoint}/#{API_VERSION}"
  end
end
