module Razoom
  class Search < Razoom::Base
    self.collection_name = ''

    def self.where(q)
      get(:search, q: q)
    end
  end
end
