module Razoom
  class Destination < Razoom::Base
    self.element_name = "city"
    self.collection_parser = Razoom::ApiCollection
  end
end
