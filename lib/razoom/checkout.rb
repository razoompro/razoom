module Razoom
  class Checkout < Razoom::Base
    def self.collection_path(prefix_options = {}, query_options = nil)
      query = ''
      query = "?#{query_options.to_query}" unless query_options.nil?

      prefix_path =
      if prefix_options[:cart_id].present?
        "/v1/carts/#{prefix_options[:cart_id]}/checkouts"
      else
        "/v1/checkouts"
      end
      "#{prefix_path}#{query}"
    end

    def self.create(args={})
      resource = new(args)
      resource.prefix_options[:cart_id] = args.delete(:cart_id)
      resource.save
      resource
    end
  end
end
