module Razoom
  class PaginatedCollection < ApiCollection
    attr_accessor :page, :page_size, :total
    def initialize(parsed = {})
      @elements = parsed['items']
      @page = parsed['paging']['page']
      @page_size = parsed['paging']['page_size']
      @total = parsed['paging']['total']
    end
  end
end
