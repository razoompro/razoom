module Razoom
  class Price < Razoom::Base
    self.collection_parser = Razoom::ApiCollection

    class << self
      def find_by_product_id(product_id, params={})
        find(:one, from: "#{Razoom.base_uri}/products/#{product_id}/prices", params: params[:params] || {})
      end

      def find_by_product(product_id, params={})
        find_by_product_id(product_id, params)
      end
    end
  end
end
