module Razoom
  class Product < Razoom::Base
    self.collection_parser = Razoom::ProductCollection
    has_one :location, class_name: 'Razoom::Location'
    has_one :price, class_name: 'Razoom::Price'
    has_many :availabilities, class_name: 'Razoom::Availability'

    def preview_image
      if respond_to?(:images)
        covers = images.select { |i| i.is_cover }
        covers.any? ? covers.first : images.first
      end
    end

    def related
      Razoom::Product.find(:all, from: "#{Razoom.base_uri}/products/#{id}/related")
    end

    def to_param
      slug.presence || id
    end
  end
end
