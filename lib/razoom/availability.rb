module Razoom
  class Availability < Razoom::Base
    self.collection_parser = Razoom::ApiCollection
    self.prefix = "/v1/products/:product_id/"
  end
end
