module Razoom
  class Customer < Razoom::Base
    self.collection_parser = Razoom::ApiCollection
    self.prefix = "/v1/carts/:cart_id/"
    self.element_name = 'customer'
    self.collection_name = 'customer'

    class << self
      def find_by_cart_id(cart_id, params={})
        find(:one, from: "#{Razoom.base_uri}/carts/#{cart_id}/customer")
      end

      def find_by_cart(cart_id, params={})
        find_by_cart_id(cart_id, params)
      end
    end

    schema do
      string 'treatment', 'first_name', 'surname', 'email', 'passport_id',
      'country', 'phone', 'phone_2', 'phone_3', 'birthdate', 'marital_status', 'chosen_pickup'
    end
  end
end
