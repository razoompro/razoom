module Razoom
  class ProductCollection < Razoom::PaginatedCollection
    attr_accessor :categories, :requested
    def initialize(parsed = {})
      super
      @categories = parsed['categories']
      @requested = parsed['requested']
    end
  end
end
