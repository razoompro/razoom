module Razoom
  class ApiCollection < ActiveResource::Collection
    def empty?
      !self.any?
    end
  end
end
