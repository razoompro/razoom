module Razoom
  class Client
    attr_accessor :token

    def initialize(options={})
      @token =
      if options[:token].present?
        options[:token]
      else
        generate_new_token(options[:name], options[:password])
      end
    end

    def headers
      Razoom::Base.set_token(@token)
    end

    def availabilities
      Razoom::Availability.set_token(@token)
    end

    def carts
      Razoom::Cart.set_token(@token)
    end

    def categories
      Razoom::Category.set_token(@token)
    end

    def checkouts
      Razoom::Checkout.set_token(@token)
    end

    def countries
      Razoom::Country.set_token(@token)
    end

    def customers
      Razoom::Customer.set_token(@token)
    end

    def destinations
      Razoom::Destination.set_token(@token)
    end

    def prices
      Razoom::Price.set_token(@token)
    end

    def products
      Razoom::Product.set_token(@token)
    end

    def search
      Razoom::Search.set_token(@token)
    end

    private
    def generate_new_token(name, password)
      @oauth_token ||= Razoom.generate_token(name, password)
      @oauth_token.token
    end
  end
end
