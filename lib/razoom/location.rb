module Razoom
  class Location
    attr_accessor :location, :persisted

    def initialize(attributes={}, persisted=false)
      @location = attributes['location']
      @persisted = persisted
    end
  end
end
