module Razoom
  class Base < ActiveResource::Base
    cattr_accessor :static_headers
    self.include_format_in_path = false
    self.collection_parser = Razoom::PaginatedCollection
    self.static_headers = headers

    def self.set_site(site)
      self.site = site
    end

    def self.set_token(token)
      @token = token
      return self
    end

    def self.token
      @token
    end

    def self.headers
      new_headers = static_headers.clone
      new_headers['Authorization'] = "Bearer #{token}"
      new_headers
    end
  end
end
