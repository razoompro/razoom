# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'razoom/version'

Gem::Specification.new do |s|
  s.add_runtime_dependency 'activeresource', '~> 4.0'
  s.add_runtime_dependency 'oauth2', '~> 1.1.0'
  s.add_development_dependency 'bundler', '~> 1.11'
  s.add_development_dependency 'rake', '~> 10.4'
  s.add_development_dependency 'rspec', '~> 3.4'
  s.add_development_dependency 'webmock', '~> 1.24'
  s.add_development_dependency 'json', '~> 1.8', '>= 1.8.3'
  s.add_development_dependency 'sinatra', '~> 1.4', '>= 1.4.7'
  s.name        = 'razoom'
  s.date        = '2016-04-14'
  s.summary     = "Biblioteca para API"
  s.description = "Biblioteca para consumir a API de tours da Razoom"
  s.authors     = ["Raphael Abreu", "Leonardo Camelo"]
  s.email       = 'devs@razoom.com.br'
  s.files       = %w(razoom.gemspec) + Dir['lib/**/*.rb']
  s.require_paths = %w(lib)
  s.homepage    = 'https://bitbucket.org/razoompro/razoom'
  s.license       = 'MIT'
  s.version     = Razoom::VERSION
end
