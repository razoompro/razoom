require 'webmock/rspec'
require 'support/fake_api'
require 'razoom'

RSpec.configure do |config|
  config.before(:each) do
    Razoom.initialize(endpoint: 'https://api.razoom.com.br',
                  api_id: '73515b9230b2aa843692cfe8396e0c15da31770af33cf9bbface1de3bd51047f',
                  secret: 'dd2f1c01af7153456f5550e852da27a19c9a3222fb946f6c2f37de58c70ecbdc')

    stub_request(:any, /api.razoom.com.br/).to_rack(FakeApi)
  end
end
