require 'sinatra/base'
require 'json'

class FakeApi < Sinatra::Base
  before do
    content_type :html, 'charset' => 'utf-8'
  end

  get '/v1/version' do
    json_response 200, 'version.json'
  end

  get '/v1/:resource/:id/:subresource' do
    json_response 200, "#{params[:subresource]}.json"
  end

  get '/v1/:resource/:id' do
    json_response 200, "#{params[:resource]}/#{params[:id]}.json"
  end

  get '/v1/:resource' do |resource|
    json_response 200, "#{resource}.json"
  end

  post "/v1/carts" do
    request.body.rewind
    data = JSON.parse request.body.read
    if data.has_key?("cart")
      json_response 201, "carts/create.json"
    else
      json_response 500, ""
    end
  end

  post '/v1/carts/:cart_id/customer' do
    request.body.rewind
    data = JSON.parse request.body.read
    json_response 201, "customer/create.json"
  end

  post "/oauth/token" do
    json_response 200, "oauth_token.json"
  end

  post "/v1/carts/:cart_id/checkouts" do
    json_response 201, "checkouts/76.json"
  end

  private

  def json_response(response_code, file_name)
    content_type :json
    status response_code
    unless file_name.blank?
      File.open(File.dirname(__FILE__).gsub('/support', '') + '/fixtures/' + file_name, 'rb:UTF-8').read
    end
  end
end
