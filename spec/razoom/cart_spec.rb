require 'spec_helper'

describe Razoom::Cart do
  it 'retorna o carrinho selecionado' do
    cart = Razoom::Cart.find(193)
    expect(cart.id).to eq(193)
  end

  it 'initializa um carrinho de compras para um produto' do
    cart = Razoom::Cart.create(:items => [{ :product_id => 2606, :departure_id => 25844650, :quantity => 1 }])
    expect(cart.id).to_not eq(nil)
    expect(cart).to respond_to(:items)
  end
end
