require 'spec_helper'

describe 'External request' do
  it 'retorna a versão da API (mock)' do
    uri = URI('https://api.razoom.com.br/v1/version')

    response = JSON.load(Net::HTTP.get(uri))

    expect(response['version']).to eq '1.0'
  end
end
