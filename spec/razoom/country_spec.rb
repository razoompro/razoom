require 'spec_helper'

describe Razoom::Country do
  it 'retorna os países existentes' do
    expect(Razoom::Country.all.length).to eq(221)
  end

  it 'deve respodender ao comando empty?' do
    countries = Razoom::Country.all
    expect(countries).to respond_to(:empty?)
  end
end
