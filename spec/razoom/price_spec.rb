require 'spec_helper'

describe Razoom::Price do
  it 'retorna o price de um produto' do
    price = Razoom::Price.find_by_product_id(1)
    expect(price.unit).to eq(187)
  end

  it 'retorna o price de um produto informando departure, quantity, child_quantity e infant_quantity' do
    price = Razoom::Price.find_by_product(1, params: { quantity: 1, child_quantity: 1,
      infant_quantity: 1, departure_id: 10 })

    expect(price.unit).to eq(187)
  end
end
