require 'spec_helper'

describe Razoom::Customer do
  it 'deve retornar o customer de um cart' do
    customer = Razoom::Customer.find_by_cart(1)
    expect(customer).to respond_to(:id, :first_name, :surname)
  end

  it 'deve permitir criar um customer para um carrinho' do
    customer = Razoom::Customer.create("cart_id" => 1, "first_name" => "Raphael", "surname" => "Abreu", "email" => "abreu@razoom.com.br" )
    expect(customer.persisted?).to eq(true)
  end

  it 'deve possuir os atributos do customer ao inicializar' do
    customer = Razoom::Customer.new
    expect(customer).to respond_to(:treatment, :first_name, :surname, :email, :passport_id,
    :country, :phone, :phone_2, :phone_3, :birthdate, :marital_status, :chosen_pickup)
  end
end
