require 'spec_helper'

describe Razoom::Category do
  it 'retorna as categorias existentes' do
    expect(Razoom::Category.all.length).to eq(14)
  end
end
