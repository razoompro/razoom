require 'spec_helper'

describe Razoom::Product do
  it 'retorna todos os produtos paginados' do
    expect(Razoom::Product.all.length).to eq(10)
  end

  it 'possui a preview_image nos produtos' do
    product = Razoom::Product.first
    expect(product).to respond_to(:preview_image)
    expect(product.preview_image.thumb_url).to eq("https://razoom-api.s3.amazonaws.com/production/2016/04/14/11/06/45/4d4523f4-005f-4b29-bc9f-6a8259ff8ba8/6009020497_2e3b557e05_b-1024x678 (1).jpg")
  end

  it 'deve possuir page, categories e requested' do
    products = Razoom::Product.all
    expect(products).to respond_to(:categories, :requested, :page)
  end

  it 'A coleção de produtos deve responder a empty?' do
    products = Razoom::Product.all
    expect(products).to respond_to(:empty?)
    expect(products.empty?).to be(false)
  end

  it 'deve conseguir buscar o produto desejado' do
    product = Razoom::Product.find(2930)
    expect(product).to respond_to(:id)
  end

  it 'deve retornar os produtos relacionados' do
    product = Razoom::Product.find(2930)
    expect(product.related.length).to eq(4)
  end

  it 'deve possuir o id ou o slug como id' do
    product = Razoom::Product.find(2930)
    expect(product.to_param).to eq("um-dia-no-rio-pao-de-acucar-e-corcovado-almoco-incluso-002930")
  end
end
