require 'spec_helper'

describe Razoom::Search do
  it 'retorna o resultado da busca pelo Rio de Janeiro' do
    result = Razoom::Search.where(q: 'Rio de Janeiro')
    expect(result).to have_key('popular_destinations')
    expect(result).to have_key('products')
  end

  it 'retorna 10 resultados para a busca pelo Rio de Janeiro' do
    result = Razoom::Search.where(q: 'Rio de Janeiro')
    expect(result['popular_destinations'].length).to eq(5)
    expect(result['products'].length).to eq(5)
  end

  it 'retorna em json os objetos quando solicitado' do
    expected_json_result = File.open(File.dirname(__FILE__).gsub('/spec/razoom', '/spec') + '/fixtures/search.json', 'rb:UTF-8').read
    result = Razoom::Search.where('Rio de Janeiro')
    expect("#{result.to_json}\n").to eq(expected_json_result)
  end
end
