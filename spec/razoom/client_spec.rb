require 'spec_helper'

describe Razoom::Client do
  it 'deve preencher os headers corretamente' do
    client = Razoom::Client.new(token: "6491c372a87e5f488d9b8ffa308ac8b2132fb69c5e7b62b1f61e03cdb6ca8d10")
    client_2 = Razoom::Client.new(token: "A")

    expect(client.categories.headers['Authorization']).to eq("Bearer 6491c372a87e5f488d9b8ffa308ac8b2132fb69c5e7b62b1f61e03cdb6ca8d10")
    expect(client_2.categories.headers['Authorization']).to eq("Bearer A")
  end

  it 'deve preencher os headers corretamente de acordo com o usuário e senha' do
    client = Razoom::Client.new(name: 'abreu@razoom.com.br', password: '12345678')

    expect(client.categories.headers['Authorization']).to eq("Bearer 0e5c6d446a705ee95fbc3030023b1ce71640b057379cf7be226e8c4b29c8e5df")
  end
end
