require 'spec_helper'

describe Razoom::Availability do
  it 'retorna as disponibilidades de um produto' do
    product = Razoom::Product.first
    expect(product.availabilities.length).to eq(10)
  end

  it 'retorna as disponibilidades de um produto antes de buscá-lo' do
    expect(Razoom::Availability.where(product_id: 1).length).to eq(10)
  end
end
