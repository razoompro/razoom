require 'spec_helper'

describe Razoom::Checkout do
  it 'retorna o checkout selecionado' do
    checkout = Razoom::Checkout.find(76)
    expect(checkout.id).to eq(76)
  end

  it 'salva um checkout' do
    checkout = Razoom::Checkout.new("token" => '12345789', "installments" => 1)
    checkout.prefix_options[:cart_id] = 193
    checkout.save
    expect(checkout).to respond_to(:id)
    expect(checkout.persisted?).to eq(true)
  end

  it 'cria um checkout' do
    checkout = Razoom::Checkout.create(cart_id: 193, token: '12345789', installments: 1)
    expect(checkout).to respond_to(:id)
    expect(checkout.persisted?).to eq(true)
  end
end
